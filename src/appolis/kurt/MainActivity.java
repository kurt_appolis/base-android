package appolis.kurt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setupView();
	}

	private void setupView() {
		
		Button button = (Button) findViewById(R.id.button);
		
		button.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.button:
			EditText editText1 = (EditText) findViewById(R.id.editText1);
			String value = editText1.getText().toString();

			if (!value.equalsIgnoreCase("")) {
				Bundle bundle = new Bundle();
				bundle.putString("name", value);

				Intent intent = new Intent(this, FireBase.class);
				intent.putExtras(bundle);

				startActivity(intent);

			} else {
				Toast.makeText(this, "There is no age", Toast.LENGTH_LONG)
						.show();
			}
			break;
		}
	}
}
