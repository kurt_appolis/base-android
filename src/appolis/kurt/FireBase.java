package appolis.kurt;

import java.util.ArrayList;
import java.util.Random;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import appolis.kurt.classes.User;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class FireBase extends AppCompatActivity implements OnClickListener {
	Firebase myFirebaseRef = null;
	TextView textView_msg = null;
	ArrayList<User> users = new ArrayList<User>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fire_base);

		setupView();

		firebaseStuff();
	}

	private void setupView() {
		String name = getIntent().getExtras().getString("name");
		if (name != null)
			getSupportActionBar().setTitle(name);

		textView_msg = (TextView) findViewById(R.id.textView_msg);

		Button button = (Button) findViewById(R.id.button);
		button.setOnClickListener(this);
	}

	private void firebaseStuff() {
		// how to:
		// https://www.firebase.com/docs/android/quickstart.html#section-install

		// step 1 : setup firebase
		Firebase.setAndroidContext(this);

		// step 2
		myFirebaseRef = new Firebase(
				"https://[change me].firebaseio.com/");

		// step 3: setup the listen to listen for any changes.
		myFirebaseRef.child("users").addValueEventListener(
				new ValueEventListener() {
					@Override
					public void onDataChange(DataSnapshot snapshot) {
						System.out.println(snapshot.getValue());
						// "Do you have data? You'll love Firebase."

						//retrieving all the users
						Iterable<DataSnapshot> users_snap = snapshot
								.getChildren();
						String final_str = "";
						
						// looping all the users
						for (DataSnapshot dataSnapshot : users_snap) {
							User temp = (User) dataSnapshot
									.getValue(User.class);
							//adding the full names and message into one string
							final_str += "\nUser: "+temp.getFullName()+" msg: "+temp.getStatus();
						}
						textView_msg.setText(final_str);
					}

					@Override
					public void onCancelled(FirebaseError error) {

						textView_msg.setText("Error retrieving details");
					}
				});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button:
			EditText message = (EditText) findViewById(R.id.message);
			String sending_str = message.getText().toString();

			if (!sending_str.equalsIgnoreCase("")) {
				Random ran = new Random();
				int random_num = ran.nextInt(1000 - 500 + 1) + 500;

				Firebase alanRef = myFirebaseRef.child("users").child(
						"user_" + random_num);

				User random_user = new User("user_" + random_num, sending_str);
				alanRef.setValue(random_user);

				myFirebaseRef.child("messages").setValue(sending_str);
			} else {
				Toast.makeText(this, "Please type in a valid message to send",
						Toast.LENGTH_LONG).show();
			}

			break;
		}
	}
}
