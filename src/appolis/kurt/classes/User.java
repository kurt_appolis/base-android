package appolis.kurt.classes;

public class User {
	
	private String fullName;
	private String status;

	public User() {
	}

	public User(String fullName, String status) {
		this.fullName = fullName;
		this.status = status;

	}


	public String getStatus(){
		return status;
	}
	public String getFullName() {
		return fullName;
	}
}
